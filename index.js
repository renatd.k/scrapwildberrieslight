const request = require('request');
const parser = require('node-html-parser');

const mainURL = "https://www.wildberries.ru/catalog/obuv/muzhskaya/botinki-i-polubotinki?page=1&xsubject=2956";
const catalogURL = mainURL.split("?")[0];

const catalogDataURL = "https://www.wildberries.ru/catalogdata";
const xDataURL = "https://wbxcatalog-ru.wildberries.ru";

function doRequest(url) {
  return new Promise(function (resolve, reject) {
    request(url, function (error, res, body) {
    if (!error && res.statusCode == 200) {
        resolve(body);
    } else {
        reject(error);
    }
    });
  });
}

async function getCatalogData(catalogName){
  const catalogURL =  `${catalogDataURL}${catalogName}`;
  const response = await doRequest(catalogURL);
  const data = JSON.parse(response);

  const catalogs = data.value.data.model.catalogMenu[0].children.map(catalog => { 
      return { name: catalog.node.name, link:catalog.node.link}
  });

  const products = data.value.data.model.products.map(product => {
      return { article: product.cod1S, name: product.name, brand: product.brand }
  })
  
  const xData = data.value.data.model.xData;
  return {catalogs, products, xData};
}

async function getProductDetale(article) {
  const url = `https://www.wildberries.ru/catalog/${article}/detail.aspx?targetUrl=ST`;
  const response = await doRequest(url);
  const description = parser.parse(response).querySelector(".j-description > p").innerText;
  const name = parser.parse(response).querySelector(".brand-and-name > .name").innerText;
  return { name, description };
}

async function getXsubjectCategories(xData) {
  const xSubjectsURL = `${xDataURL}/${xData.xcatalogShard}/filters?filters=xsubject&locale=ru&${xData.xcatalogQuery}`;
  const xSubjectsData = await doRequest(xSubjectsURL);
  const items = JSON.parse(xSubjectsData).data.filters[0].items;
  return items.map(xsubject => {
    return { name: xsubject.name, link: `${catalogURL}?page=1&xsubject=${xsubject.id}` }
  })
}

async function getXsubjectProducts(xData, xSubjectId) {
  const xSubjectProductURL = `${xDataURL}/${xData.xcatalogShard}/catalog?page=1&locale=ru&xsubject=${xSubjectId}`;
  const xSubjectsProductData = await doRequest(xSubjectProductURL);
  const products = JSON.parse(xSubjectsProductData).data.products;
  return products.map(product => {
    return { article: product.id, name: product.name, brand: product.brand }
  })
}

(async () => {
  try {
    const catalogName = catalogURL.split("catalog")[1];
    const catalogData = await getCatalogData(catalogName);
    const xSubjectsCategories = await getXsubjectCategories(catalogData.xData);

    if (catalogName == catalogData.catalogs[0].link.split("catalog")[1].split("?")[0]) {
      catalogData.catalogs = xSubjectsCategories;
      if (mainURL.includes("xsubject")) {
        const xSubjectID = mainURL.split("xsubject=")[1].split("&")[0]; 
        const xSubjectProducts = await getXsubjectProducts(catalogData.xData, xSubjectID);
        catalogData.products = xSubjectProducts;
      }
    }
    delete catalogData.xData;
    console.log("catalogs:", catalogData.catalogs);

    let position = 1;
    for (const productIndex in catalogData.products) {
      const article = catalogData.products[productIndex].article;
      const productData = await getProductDetale(article);
      
      catalogData.products[productIndex].position = position++;
      catalogData.products[productIndex].nameOnSite = productData.name;
      catalogData.products[productIndex].description = productData.description;
      
      console.log("product:", catalogData.products[productIndex]);
    }
    console.log("result:", catalogData);
  } catch (error) {
    console.log(error);
  }


})();